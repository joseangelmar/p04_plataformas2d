using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public bool saltarinas;
    private Rigidbody2D rb2d;
    public GameObject chest;
    
    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        if (saltarinas == true)
        {
            gameObject.transform.position = chest.transform.position+new Vector3(0,1,0);
            rb2d.velocity = new Vector2(rb2d.velocity.x, Random.Range(-3, 3));
            rb2d.velocity = new Vector2(rb2d.velocity.y, 5);
        }
    }
    private void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.monedas++;
            Destroy(gameObject);
        }
    }
}
