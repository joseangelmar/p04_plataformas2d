using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercontroler : MonoBehaviour
{
    public float maxSpeed = 5f;
    public float speed = 15f;
    public float jumpForce = 5f;
    public bool isGrounded;
    private bool keyplus;
    private SpriteRenderer mySpriteRenderer;
    private Rigidbody2D rb2d;
    private Animator anim;
    private bool callendo;  
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        anim.SetBool("Ground", isGrounded);
        anim.SetBool("teclaspulsadas", keyplus);
        anim.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("Speedy", Mathf.Abs(rb2d.velocity.y));
        anim.SetBool("Callendo", callendo);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.05f, LayerMask.GetMask("floor"));
        if ((hit.collider != null) && (hit.collider.CompareTag("floor")))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        if (Input.anyKey)
        {
            keyplus = true;
        }
        else
        {
            keyplus = false;

        }
        float h = Input.GetAxis("Horizontal");
        if ((h == 0) && (isGrounded == true) && (keyplus == false))
        {
           rb2d.velocity = Vector2.zero;
        }
        else
        {
            rb2d.AddForce(Vector2.right * speed * h);

            float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
            rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded==true)
            {               
                salto();               
            }            
        }  
        if (rb2d.velocity.x > 0)
        {
            mySpriteRenderer.flipX = false;
        }
        if (rb2d.velocity.x < 0)
        {
            mySpriteRenderer.flipX = true;
        }
        if (rb2d.velocity.y < 0)
        {
            callendo = true;
        }
        if (rb2d.velocity.y > 0)
        {
            callendo = false;
        }
    }
    private void salto()
    {       
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
        isGrounded = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("enemy"))
        {
            Destroy(gameObject);
            GameManager.muerto = true;
        }
    }
}
