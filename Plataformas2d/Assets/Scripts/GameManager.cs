using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int monedas;
    public Text coinText;
    public static bool muerto;
    public GameObject gameOver;
    // Start is called before the first frame update
    void Start()
    {
        gameOver.SetActive(false);
        muerto = false;
    }

    // Update is called once per frame
    void Update()
    {
        coinText.text = ("Monedas ")+monedas.ToString();
        if (muerto == true)
        {
            muerte();
        }

    }
    public void muerte()
    {
        gameOver.SetActive(true);
    }
    public void reestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
