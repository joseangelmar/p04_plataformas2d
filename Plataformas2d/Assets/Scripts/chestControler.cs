using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chestControler : MonoBehaviour
{
    private Animator anim;
    private bool playerin;
    public GameObject monedas;
    public int cantidad;
    // Start is called before the first frame update
    void Start()
    {
        playerin = false;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("playerin", playerin);
        if (playerin == true && (Input.GetKeyDown(KeyCode.W)))
        {
            StartCoroutine(UsingYield(1));
            
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerin = false;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerin = true;
        }
    }
    IEnumerator UsingYield(int seconds)
    {
        for (int i = 0; i < cantidad; i++)
        {
            yield return new WaitForSeconds(0.25f);
            Instantiate(monedas);
        }
        
    }
}
